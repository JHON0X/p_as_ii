<?php
  include 'cn.php';
  session_start();
  $varsesion =$_SESSION['user'];
  if($varsesion == null || $varsesion=''){
  header ('location:a_sesion.php');
  die();
}
?>



<?php 
include 'cn.php';

    //vender pre-procesado
    $pre_grande= 100;
    $pre_mediano= 80;
    $pre_peque= 70;
    //vender procesado
    $pro_grande= 105;
    $pro_mediano= 85;
    $pro_peque= 75;
    //vender cartera
    $cart_rosa= 70;
    $cart_cafe= 50;
    //Comprar fresco
    $fres_grande= 85;
    $fres_mediano= 70;
    $fres_peque= 65;
?>

    <!DOCTYPE html>
	<html lang="es" class="no-js">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta charset="UTF-8">
		<title>Admin</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/animate.min.css">
			<link rel="stylesheet" href="css/owl.carousel.css">
			<link rel="stylesheet" href="css/main.css">
            <link rel="stylesheet" href="css/compra.css">
            <script>
            function Calcular() {
                        document.getElementById("pre_grande").value = <?php echo $pre_grande;?>;
                        document.getElementById("pre_mediano").value = <?php echo $pre_mediano;?>;
                        document.getElementById("pre_peque").value = <?php echo $pre_peque;?>;
                        document.getElementById("pro_grande").value = <?php echo $pro_grande;?>;
                        document.getElementById("pro_mediano").value = <?php echo $pro_mediano;?>;
                        document.getElementById("pro_peque").value = <?php echo $pro_peque;?>;
                        document.getElementById("cart_rosa").value = <?php echo $cart_rosa;?>;
                        document.getElementById("cart_cafe").value = <?php echo $cart_cafe;?>;
                        document.getElementById("fres_grande").value = <?php echo $fres_grande;?>;
                        document.getElementById("fres_mediano").value = <?php echo $fres_mediano;?>;
                        document.getElementById("fres_peque").value = <?php echo $fres_peque;?>;

                        
            }
            </script>
            
		</head>
		<body style="background: url(img/precue.jpg);background-size: cover;">
			
			<section class="banner-area" id="home" style="background: rgba(0,0,0,0.5)">
				
				<header class="default-header">
					<nav class="navbar navbar-expand-lg  navbar-light">
						<div class="container">
							  <a class="navbar-brand" href="index.html">
							  	<img src="img/logo.png" alt="">
							  </a>
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							    <span class="text-white lnr lnr-menu"></span>
							  </button>

							  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
							    <ul class="navbar-nav">
                                    <li><a href="index.html">INICIO</a></li>
                                    <li class="dropdown">
								      <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
								        Tabla de Cueros
								      </a>
								      <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#pre_procesados">Pre-Procesados</a>
                                        <a class="dropdown-item" href="#procesados">Procesados</a>
                                        <a class="dropdown-item" href="#carteras">carteras</a>
								        <a class="dropdown-item" href="#frescos">frescos</a>
								      </div>
								    </li>
                                    <li><a href="cerrar.php">Salir</a></li>
									
							    </ul>
							  </div>						
						</div>
					</nav>
				</header>
			</section>
		

			
				
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Precios de Compra/Venta</h1>
                                <div class="row col-lg-12">
                                    <div class="container col-lg-3 text-left" >
                                    
                                    <h3 style="color:white;">Precio del cuero Pre-procesado</h3>
                                    <br>
                                    
                                    <span >Precio Actual:</span>
                                   <br>
                                   <div class="input-group">
                                   <span class="btn btn-default">Grande:</span>
                                    <input type="text" class="form-control" name="pre_grande" id="pre_grande" readonly  onclick="Calcular();"></div><br>
                                    <div class="input-group">
                                    <span class="btn btn-default">Mediano:</span>
                                     <input type="text" class="form-control" name="pre_mediano" id="pre_mediano" readonly onclick="Calcular();"></div><br>
                                     <div class="input-group"> 
                                     <span class="btn btn-default">Pequeño:</span>
                                     <input type="text" class="form-control" name="pre_peque" id="pre_peque" readonly onclick="Calcular();"></div>
                                    
                                    
                                    </div>

                                    <div class="container col-lg-3 text-left" >
                                    <h3 style="color:white;">Precio del Cuero Procesado</h3>
                                    <br>
                                    <span >Precio Actual:</span>
                                   <br>
                                   <div class="input-group">
                                   <span class="btn btn-default">Grande:</span>
                                    <input type="text" class="form-control" name="pro_grande" id="pro_grande" readonly onclick="Calcular();"></div><br>
                                    <div class="input-group">
                                    <span class="btn btn-default">Mediano:</span>
                                     <input type="text" class="form-control" name="pro_mediano" id="pro_mediano" readonly onclick="Calcular();"></div><br>
                                     <div class="input-group"> 
                                     <span class="btn btn-default">Pequeño:</span>
                                     <input type="text" class="form-control" name="pro_peque" id="pro_peque" readonly onclick="Calcular();"></div>
                                    </div>
                                    
                                    <div class="container col-lg-3 text-left" >
                                    <h3 style="color:white;">Precio de las Carteras</h3>
                                    <br>
                                    <span >Precio Actual:</span>
                                   <br>
                                   <div class="input-group">
                                   <span class="btn btn-default">Rosa:</span>
                                    <input type="text" class="form-control" name="cart_rosa" id="cart_rosa" readonly onclick="Calcular();"></div><br>
                                    <div class="input-group">
                                    <span class="btn btn-default">Cafe:</span>
                                     <input type="text" class="form-control" name="cart_cafe" id="cart_cafe" readonly onclick="Calcular();"></div><br>
                                    
                                    
                                    
                                    </div>
                                    <div class="container col-lg-3 text-left" >
                                    <h3 style="color:white;">Precio de Comprar del Cuero Fresco</h3>
                                    <br>
                                    <span >Precio Actual:</span>
                                   <br>
                                   <div class="input-group">
                                   <span class="btn btn-default">Grande:</span>
                                    <input type="text" class="form-control" name="fres_grande" id="fres_grande" readonly onclick="Calcular();"></div><br>
                                    <div class="input-group">
                                    <span class="btn btn-default">Mediano:</span>
                                     <input type="text" class="form-control" name="fres_mediano" id="fres_mediano" readonly onclick="Calcular();"></div><br>
                                     <div class="input-group"> 
                                     <span class="btn btn-default">Pequeño:</span>
                                     <input type="text" class="form-control" name="fres_peque" id="fres_peque" readonly onclick="Calcular();"></div>
                                    </div>
                            </div>
                        </div>
                        </div>
                     <br>

                <div class="container" id="pre_procesados">
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Registros de Venta de Cueros Pre-procesados</h1>
                                <div class="row col-lg-12">
                                    <div class="table table-responsive table-hover" >
                                    <table>
                                    <tr >
                                        <td scope="col" >NOMBRES</td>
                                        <td scope="col" >CI/CODIGO</td>
                                        <td scope="col" >CORREO</td>
                                        <td scope="col" >GRANDES</td>
                                        <td scope="col" >MEDIANOS</td>
                                        <td scope="col" >PEQUEÑOS</td>
                                        <td scope="col" >FECHA</td>
                                        <td scope="col" >TOTAL</td>
                                    </tr>
                                    <?php
                                    $sql="SELECT * FROM compra_pre";
                                    $result= mysqli_query($conexion, $sql);
                                    while($mostrar=mysqli_fetch_array($result)){
                                ?>
                                    <tr scope="row">
                                        <td class="tipos"><?php echo $mostrar['nombre']?></td>
                                        <td class="tipos"><?php echo $mostrar['ci']?></td>
                                        <td class="tipos"><?php echo $mostrar['correo']?></td>
                                        <td class="tipos"><?php echo $mostrar['grande']?></td>
                                        <td class="tipos"><?php echo $mostrar['mediano']?></td>
                                        <td class="tipos"><?php echo $mostrar['peque']?></td>
                                        <td class="tipos"><?php echo $mostrar['fecha']?></td>
                                        <td class="tipos"><?php echo $mostrar['total']."Bs"?></td>
                                    </tr>   
                                    <?php
                                    }
                                ?>
                                </table>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="" id="procesados"></div>
                    <div class="container" >
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Registros de Venta de Cueros Procesados</h1>
                                <div class="row col-lg-12">
                                    <div class="table table-responsive table-hover" >
                                    <table>
                                    <tr >
                                        <td scope="col" >NOMBRES</td>
                                        <td scope="col" >CI/CODIGO</td>
                                        <td scope="col" >CORREO</td>
                                        <td scope="col" >GRANDES</td>
                                        <td scope="col" >MEDIANOS</td>
                                        <td scope="col" >PEQUEÑOS</td>
                                        <td scope="col" >FECHA</td>
                                        <td scope="col" >TOTAL</td>
                                    </tr>
                                    <?php
                                    $sql="SELECT * FROM compra_proce";
                                    $result= mysqli_query($conexion, $sql);
                                    while($mostrar=mysqli_fetch_array($result)){
                                ?>
                                    <tr scope="row">
                                        <td class="tipos"><?php echo $mostrar['nombre']?></td>
                                        <td class="tipos"><?php echo $mostrar['ci']?></td>
                                        <td class="tipos"><?php echo $mostrar['correo']?></td>
                                        <td class="tipos"><?php echo $mostrar['grande']?></td>
                                        <td class="tipos"><?php echo $mostrar['mediano']?></td>
                                        <td class="tipos"><?php echo $mostrar['peque']?></td>
                                        <td class="tipos"><?php echo $mostrar['fecha']?></td>
                                        <td class="tipos"><?php echo $mostrar['total']."Bs"?></td>
                                    </tr>   
                                    <?php
                                    }
                                ?>
                                </table>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div id="carteras"></div>
                <div class="container" >
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Registros de Venta de Carteras</h1>
                                <div class="row col-lg-12">
                                    <div class="table table-responsive table-hover" >
                                    <table>
                                    <tr >
                                        <td scope="col" >NOMBRES</td>
                                        <td scope="col" >CI/CODIGO</td>
                                        <td scope="col" >CORREO</td>
                                        <td scope="col" >ROSA</td>
                                        <td scope="col" >CAFE</td>
                                        <td scope="col" >FECHA</td>
                                        <td scope="col" >TOTAL</td>
                                    </tr>
                                    <?php
                                    $sql="SELECT * FROM compra_cartera";
                                    $result= mysqli_query($conexion, $sql);
                                    while($mostrar=mysqli_fetch_array($result)){
                                ?>
                                    <tr scope="row">
                                        <td class="tipos"><?php echo $mostrar['nombre']?></td>
                                        <td class="tipos"><?php echo $mostrar['ci']?></td>
                                        <td class="tipos"><?php echo $mostrar['correo']?></td>
                                        <td class="tipos"><?php echo $mostrar['rosa']?></td>
                                        <td class="tipos"><?php echo $mostrar['cafe']?></td>
                                        <td class="tipos"><?php echo $mostrar['fecha']?></td>
                                        <td class="tipos"><?php echo $mostrar['total']."Bs"?></td>
                                    </tr>   
                                    <?php
                                    }
                                ?>
                                </table>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                    </div>
                <div id="frescos"></div>
                <div class="container" >
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Tabla de registros de Cueros Frescos Comprados</h1>
                                <div class="row col-lg-12">
                                    <div class="table table-responsive table-hover" >
                                    <table>
                                    <tr >
                                        <td scope="col" >NOMBRES</td>
                                        <td scope="col" >CI/CODIGO</td>
                                        <td scope="col" >CORREO</td>
                                        <td scope="col" >GRANDES</td>
                                        <td scope="col" >MEDIANOS</td>
                                        <td scope="col" >PEQUEÑOS</td>
                                        <td scope="col" >FECHA</td>
                                        <td scope="col" >TOTAL</td>
                                    </tr>
                                    <?php
                                    $sql="SELECT * FROM venta_fresco";
                                    $result= mysqli_query($conexion, $sql);
                                    while($mostrar=mysqli_fetch_array($result)){
                                ?>
                                    <tr scope="row">
                                        <td class="tipos"><?php echo $mostrar['nombre']?></td>
                                        <td class="tipos"><?php echo $mostrar['ci']?></td>
                                        <td class="tipos"><?php echo $mostrar['correo']?></td>
                                        <td class="tipos"><?php echo $mostrar['grande']?></td>
                                        <td class="tipos"><?php echo $mostrar['mediano']?></td>
                                        <td class="tipos"><?php echo $mostrar['peque']?></td>
                                        <td class="tipos"><?php echo $mostrar['fecha']?></td>
                                        <td class="tipos"><?php echo $mostrar['total']."Bs"?></td>
                                    </tr>   
                                    <?php
                                    }
                                ?>
                                    </table>
                                    </div>  
                                </div>
                             </div>
                        </div>
                    </div>
				
				



	
			
	
			
					

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="js/vendor/bootstrap.min.js"></script>
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
			<script src="js/owl.carousel.min.js"></script>			
			<script src="js/jquery.sticky.js"></script>
			<script src="js/slick.js"></script>
			<script src="js/jquery.counterup.min.js"></script>
			<script src="js/waypoints.min.js"></script>		
			<script src="js/main.js"></script>	
            <script src="js/validar_compra_pre.js"></script>
		</body>
	</html>