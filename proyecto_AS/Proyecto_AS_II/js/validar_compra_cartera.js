
function validar(){
    var nombre, apellido, ci, correo, rosa, cafe, fecha, expresion, exnomb;
    nombre = document.getElementById("nombre").value;
    apellido = document.getElementById("apellido").value;
    ci = document.getElementById("ci").value;
    correo = document.getElementById("correo").value;
    rosa = document.getElementById("precio_ro").value;
    cafe = document.getElementById("precio_cafe").value;
    fecha = document.getElementById("fecha").value;
    expresion = /\w+@\w+\.+[a-z]/;

    exnomb =/^[A-Za-z\_\-\.\s\xF1\xD1]+$/;

    
    if (isNaN(ci)) {
        alert("El documento de identidad tiene que ser un numero");
        return false;
    }
    if (isNaN(rosa) || isNaN(cafe) ) {
       alert("La Cantidad de Carteras tiene que ser un numero");
        return false;
    }
    if (nombre === "" || apellido === "" || ci === "" || correo === "" || fecha === "" || rosa === "" || cafe === "" ){
      alert("Rellene todos los Campos");
       return false;
    }
    if (!expresion.test(correo)) {
        alert("El CORREO ES INCORRECTO");
        return false;
   }
    if (!exnomb.test(nombre) || !exnomb.test(apellido) ) {
        alert("El NOMBRE y APELLIDO tiene que ser solo en letras");
        return false;
    }
    
}

