<?php 
    $pre_cartera_ro= 70;
    $precio_cartera_cafe= 50;
   
?>
    <!DOCTYPE html>
	<html lang="es" class="no-js">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta charset="UTF-8">
		<title>Comprar Carteras</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/animate.min.css">
			<link rel="stylesheet" href="css/owl.carousel.css">
			<link rel="stylesheet" href="css/main.css">
            <link rel="stylesheet" href="css/compra.css">
            
            <script>
            function Calcular() {
                        var precio_ro, precio_cafe, sumas, s_precio_ro, s_precio_cafe;
                        precio_ro = document.getElementById("precio_ro").value;
                        precio_cafe = document.getElementById("precio_cafe").value;
                        if (isNaN(precio_ro)) {
                             precio_ro=0;
                        }
                        if (isNaN(precio_cafe)) {
                             precio_cafe=0;
                        }

                        s_precio_ro= precio_ro*<?php echo $pre_cartera_ro;?>;
                        s_precio_cafe= precio_cafe*<?php echo $precio_cartera_cafe;?>;
                        
                        sumas= s_precio_ro + s_precio_cafe+" Bs";
                        document.getElementById("total").value = sumas;
                        document.getElementById("rosa_bs").value = <?php echo $pre_cartera_ro;?>;
                        document.getElementById("cafe_bs").value = <?php echo $precio_cartera_cafe;?>;
            }
        </script>
		</head>
		<body style="background: url(img/cuerofond.jpg);background-size: cover;">
			
			<section class="banner-area" id="home" style="background: rgba(0,0,0,0.5)">
				
				<header class="default-header">
					<nav class="navbar navbar-expand-lg  navbar-light">
						<div class="container">
							  <a class="navbar-brand" href="index.html">
							  	<img src="img/logo.png" alt="">
							  </a>
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							    <span class="text-white lnr lnr-menu"></span>
							  </button>

							  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
							    <ul class="navbar-nav">
									<li><a href="index.html">INICIO</a></li>
                                    <li><a href="compra.php">Volver</a></li>
									
							    </ul>
							  </div>						
						</div>
					</nav>
				</header>
			</section>
		

			
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Compra de Carteras</h1>
                                <div class="row col-lg-12">
									<div class="col-lg-4 ">
                                    <img src="img/cartera2.jpg">
                                    </div>
                                    <div class="col-lg-4" >
                                    <img src="img/cartera3.jpg">
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="">
                                        <dl class="text-left">
                                            <dt>Descripciones:</dt>
                                            <dd>Actual mente la Empresa esta empezando con la fabricación de Carteras con los siguientes tipos:</dd>
                                            <dt>Tipos de Cartera:</dt>
                                            <dd>La Empresa cuenta con 2 tipos de carteras:</dd>
                                            <dd>La primera de color Rosa con un diseño discreto</dd>
                                            <dd>La segunda de color Cafe con Blanco con un diseño mas funcional </dd>
                                            
                                            </dl>
                                        </div>
                                    </div>
                                </div>
						</div>
					</div>					
				</div>
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Para la compra porfavor Rellene los siguientes datos:</h1>
                                <div class="row col-lg-12">
                                    <div class="container col-lg-6">
                                    <form action="re_compra_cartera.php" class="form-horizontal" method="POST" onsubmit="return validar();">
                                        <div class="input-group">
                                        <span class="input-group-addon">Nombres:</span>
                                         <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombres...."maxlength="30">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Apellidos:</span>
                                         <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apeliidos...."maxlength="80">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">CI:</span>
                                         <input type="text" class="form-control" name="ci" id="ci" placeholder="Ci/Documento de Identidad...."maxlength="12">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Correo @:</span>
                                         <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo...."maxlength="100" >
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Cantidad de Carteras</span><br>
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Rosa:</span>
                                         <input type="text" class="form-control" name="precio_ro" id="precio_ro" placeholder="Cantidad de Carteras ...." onkeyup="Calcular();" onclick="Calcular();">
                                         <span class="input-group-addon">$:</span>
                                         <input type="text" name="rosa_bs" id="rosa_bs" class="form-control" readonly>
                                         
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Cafe:</span>
                                         <input type="text" class="form-control" name="precio_cafe" id="precio_cafe" placeholder="Cantidad de Carteras ...." onkeyup="Calcular();" onclick="Calcular();">
                                         <span class="input-group-addon">$:</span>
                                         <input type="text" name="cafe_bs" id="cafe_bs" class="form-control" readonly>
                                         
                                        </div>
                                        
                                     
                                        <br>
                                        
                                        <div class="input-group">
                                        <button class="btn btn-primary" onclick="Calcular();" type="button">Total :</button> 
                                        <input type="text" class="form-control col-lg-6" id="total" name="total" readonly>
                                        </div>
                                        <br>
                                        <input type="submit" class="btn btn-success" value="Enviar►">
                                        <button class="btn btn-warning" type="reset">Borrar▼</button>
                                    </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                



	
			
	
			
					

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="js/vendor/bootstrap.min.js"></script>
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
			<script src="js/owl.carousel.min.js"></script>			
			<script src="js/jquery.sticky.js"></script>
			<script src="js/slick.js"></script>
			<script src="js/jquery.counterup.min.js"></script>
			<script src="js/waypoints.min.js"></script>		
			<script src="js/main.js"></script>	
            <script src="js/validar_compra_cartera.js"></script>
		</body>
	</html>