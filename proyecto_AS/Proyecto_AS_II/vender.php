<?php 
    $pre_grande= 85;
    $pre_mediano= 70;
    $pre_peque= 65;
?>
	<!DOCTYPE html>
	<html lang="es" class="no-js">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta charset="UTF-8">
		<title>Vender</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/animate.min.css">
			<link rel="stylesheet" href="css/owl.carousel.css">
			<link rel="stylesheet" href="css/main.css">
			<link rel="stylesheet" href="css/compra.css">
			<script>
            function Calcular() {
                        var grande, mediano, peque, sumas, s_grande, s_mediano, s_peque;
                        grande = document.getElementById("grande").value;
                        mediano = document.getElementById("mediano").value;
                        peque = document.getElementById("peque").value;

                         if (isNaN(grande)) {
                             grande=0;
                        }
                        if (isNaN(mediano)) {
                             mediano=0;
                        }
                        if (isNaN(peque)) {
                             peque=0;
                        }

                        s_grande= grande*<?php echo $pre_grande;?>;
                        s_mediano= mediano*<?php echo $pre_mediano;?>;
                        s_peque= peque*<?php echo $pre_peque;?>;
                        sumas= s_grande + s_mediano+ s_peque+" Bs";
                        document.getElementById("total").value = sumas;
                        document.getElementById("grande_bs").value = <?php echo $pre_grande;?>;
                        document.getElementById("mediano_bs").value = <?php echo $pre_mediano;?>;
                        document.getElementById("peque_bs").value = <?php echo $pre_peque;?>;
            }
			</script>
		</head>
		<body style="background: url(img/precue.jpg);background-size: cover;">
			
			<section class="banner-area" id="home" style="background: rgba(0,0,0,0.5)">
				
				<header class="default-header">
					<nav class="navbar navbar-expand-lg  navbar-light">
						<div class="container">
							  <a class="navbar-brand" href="index.html">
							  	<img src="img/logo.png" alt="">
							  </a>
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							    <span class="text-white lnr lnr-menu"></span>
							  </button>

							  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
							    <ul class="navbar-nav">
									<li><a href="index.html">INICIO</a></li>
									<li><a href="compra.php">Comprar</a></li>
									
							    </ul>
							  </div>						
						</div>
					</nav>
				</header>
			</section>
		

			
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
								<h1 style="color:white;">Venta de Cueros a la Empresa</h1>
								<div class="row col-lg-12">
									<div class="col-lg-6" >
                                    <img src="img/venta_fresco.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="">
                                        <dl class="text-left">
                                            <dt>Descripciones:</dt>
                                            <dd>Actualmente la Empresa compra Cueros por mayor y menor, donde se le esta otorgando la informacion si esta interesado de trabajar con nosotros:</dd>
                                            <dt>Tamaño de Cueros:</dt>
                                            <dd>OJO todo Cuero tiene q ser fresco no pasar mas de 1 dia de faenado</dd>
                                            <dd>Grandes: Tienen un Tamaño de 2.5 a 3.5 metros</dd>
                                            <dd>Medianos: Tienen un Tamaño de 2 hasta los 2.5 metros</dd>
                                            <dd>Pequeños: Los pequeños son de 1.8 hasta los 2 metros</dd>
                                            <dt>Precio de los Cueros:</dt>
                                            <dd>El precio suele variar dependiendo de la gestion pero el precio se dara a conocer al momento del llenado de formulario</dd>
                                            <dd>Grandes: Para los grandes la venta esta de unos 85-100bs</dd>
                                            <dd>Medianos: Para los medianos la venta esta en 65-80bs</dd>
											<dd>Pequeños: Para los pequeños la venta esta en 55-62bs</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
						</div>
					</div>					
				</div>
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
                                <h1 class="mb-10" style="color:white;">Para Realizar la Venta porfavor Rellene los siguientes datos:</h1>
                                <div class="row col-lg-12">
                                    <div class="container col-lg-6">
                                    <form action="reg_venta.php" class="form-horizontal" method="POST" onsubmit="return validar();">
                                        <div class="input-group">
                                        <span class="input-group-addon">Nombres:</span>
                                         <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombres....">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Apellidos:</span>
                                         <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apeliidos....">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">CI:</span>
                                         <input type="text" class="form-control" name="ci" id="ci" placeholder="Ci/Documento de Identidad....">
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Correo @:</span>
                                         <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo...." >
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon" >Empresa:</span>
                                         <input type="text" class="form-control" name="empresa" id="empresa" placeholder="Nombre o Nombre de la Empresa....">
                                        </div>
                                        <br>
                                            
                                        <div class="input-group">
                                        <span class="input-group-addon">Cantidad y Tamaño de Cueros</span><br>
                                        </div>
                                        
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Grandes:</span>
                                         <input type="text" class="form-control" name="grande" id="grande" placeholder="Cantidad de Cueros ...." onkeyup="Calcular();" onclick="Calcular();">
                                         <span class="input-group-addon">$:</span>
                                         <input type="text" name="grande_bs" id="grande_bs" class="form-control" readonly>
                                         
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Medianos:</span>
                                         <input type="text" class="form-control" name="mediano" id="mediano" placeholder="Cantidad de Cueros ...." onkeyup="Calcular();" onclick="Calcular();">
                                         <span class="input-group-addon">$:</span>
                                         <input type="text" name="mediano_bs" id="mediano_bs" class="form-control" readonly>
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-addon">Pequeños:</span>
                                         <input type="text" class="form-control" name="peque" id="peque" placeholder="Cantidad de Cueros ...." onkeyup="Calcular();" onclick="Calcular();">
                                         <span class="input-group-addon">$:</span>
                                         <input type="text" name="peque_bs" id="peque_bs" class="form-control" readonly>
                                        </div>
                                     
                                        <br>
                                        
                                        <div class="input-group">
                                        <button class="btn btn-primary" onclick="Calcular();" type="button">Total :</button> 
                                        <input type="text" class="form-control col-lg-6" id="total" name="total" readonly>
                                        </div>
                                        <br>
                                        <input type="submit" class="btn btn-success" value="Enviar►">
                                        <button class="btn btn-warning" type="reset">Borrar▼</button>
                                    </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
				



	
			
	
			
					

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="js/vendor/bootstrap.min.js"></script>
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
			<script src="js/owl.carousel.min.js"></script>			
			<script src="js/jquery.sticky.js"></script>
			<script src="js/slick.js"></script>
			<script src="js/jquery.counterup.min.js"></script>
			<script src="js/waypoints.min.js"></script>		
			<script src="js/main.js"></script>	
			<script src="js/validar_compra_pre.js"></script>
		</body>
	</html>